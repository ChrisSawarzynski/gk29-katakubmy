#include "Camera.h"

Camera::Camera()
{
	cameraPosition = glm::vec3(0.0f, 0.0f, 20.0f);
	cameraFrontVector = glm::vec3(0.0f, 0.0f, -1.0f);
	cameraUpDirectionVector = glm::vec3(0.0f, 1.0f, 0.0f);

	fov = 50.0f;

	horizontalAngle = glm::radians(-90.0f);
	verticalAngle = glm::radians(0.0f); 
	updateCameraFrontVector();
}

glm::mat4 Camera::getLookAtMatrix()
{
	return glm::lookAt(
		cameraPosition,
		cameraPosition + cameraFrontVector,
		cameraUpDirectionVector
	);
}

glm::vec4 Camera::getPosition()
{
	return glm::vec4(cameraPosition, 1.0f);
}

glm::mat4 Camera::getPerspective(float aspectRatio)
{
	return glm::perspective(glm::radians(fov), aspectRatio, 0.01f, 100.0f);
}

void Camera::moveForward(float change)
{
	cameraPosition += cameraFrontVector * change;
}

void Camera::moveSideways(float change)
{
	cameraPosition += glm::normalize(glm::cross(cameraFrontVector, cameraUpDirectionVector)) * change;
}

void Camera::moveUp(float change)
{
	cameraPosition += cameraUpDirectionVector * change;
}

void Camera::rotateUp(float change)
{
	verticalAngle += change;
	updateCameraFrontVector();
}

void Camera::rotateSideways(float change)
{
	horizontalAngle += change;
	updateCameraFrontVector();
}

void Camera::changeFov(float value)
{
	fov = value;
}

void Camera::updateCameraFrontVector()
{
	cameraFrontVector.x = cos(horizontalAngle) * cos(verticalAngle);
	cameraFrontVector.y = sin(verticalAngle);
	cameraFrontVector.z = sin(horizontalAngle) * cos(verticalAngle);
}
