/*
Niniejszy program jest wolnym oprogramowaniem; mo�esz go
rozprowadza� dalej i / lub modyfikowa� na warunkach Powszechnej
Licencji Publicznej GNU, wydanej przez Fundacj� Wolnego
Oprogramowania - wed�ug wersji 2 tej Licencji lub(wed�ug twojego
wyboru) kt�rej� z p�niejszych wersji.

Niniejszy program rozpowszechniany jest z nadziej�, i� b�dzie on
u�yteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domy�lnej
gwarancji PRZYDATNO�CI HANDLOWEJ albo PRZYDATNO�CI DO OKRE�LONYCH
ZASTOSOWA�.W celu uzyskania bli�szych informacji si�gnij do
Powszechnej Licencji Publicznej GNU.

Z pewno�ci� wraz z niniejszym programem otrzyma�e� te� egzemplarz
Powszechnej Licencji Publicznej GNU(GNU General Public License);
je�li nie - napisz do Free Software Foundation, Inc., 59 Temple
Place, Fifth Floor, Boston, MA  02110 - 1301  USA
*/

#include "model.h"
#include <string>
#include <iostream>
#include <OBJ_Loader.h>
#include <lodepng.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <shaderprogram.h>

#define _CRT_SECURE_NO_WARNINGS 1 

namespace Models {
	Model::Model(
		std::vector<GLfloat> vertexData,
		unsigned int vertexCount,
		GLuint vertexBufferObject,
		GLuint vertexArrayObject,
		GLuint texture,
		GLuint textureUnit
	) :
		vertexData(vertexData),
		vertexCount(vertexCount),
		vertexBufferObject(vertexBufferObject),
		vertexArrayObject(vertexArrayObject),
		texture(texture),
		textureUnit(textureUnit),
		M(glm::mat4(1.0f))
	{}

	Model::~Model()
	{
		glDeleteTextures(1, &texture);
	}

	void Model::draw(ShaderProgram* sp, glm::mat4 P, glm::mat4 V)
	{
		sp->use();
		glUniformMatrix4fv(sp->u("P"), 1, false, glm::value_ptr(P));
		glUniformMatrix4fv(sp->u("V"), 1, false, glm::value_ptr(V));
		glUniformMatrix4fv(sp->u("M"), 1, false, glm::value_ptr(M));

		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, texture);

		glUniform1i(sp->u("textureMap"), textureUnit);

		bind();
		glDrawArrays(GL_TRIANGLES, 0, vertexCount);
		unbind();
	}

	glm::mat4 Model::getModelMatrix()
	{
		return M;
	}

	void Model::setModelMatrix(glm::mat4 M)
	{
		this->M = M;
	}

	void Model::bind()
	{
		glBindVertexArray(vertexArrayObject);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	void Model::unbind()
	{
		glBindVertexArray(0);
	}
	   
	ModelBuilder::ModelBuilder(const char* modelPath, std::vector<const char*> texturePaths) :
		modelPath(modelPath), texturePaths(texturePaths)
	{
	}

	std::vector<Model*> ModelBuilder::build()
	{
		if (!loadVertexData()) {
			std::cout << "Cannot load model from path: " << this->modelPath << std::endl;
		}

		return models;
	}

	bool ModelBuilder::loadVertexData()
	{
		objl::Loader Loader;
		bool loadout = Loader.LoadFile(modelPath);

		if (!loadout) {
			std::cout << "Cannot load file: " << modelPath << std::endl;
			return false;
		}


		GLuint baseTextureUnit = 0;

		for (int i = 0; i < Loader.LoadedMeshes.size(); i++)
		{
			std::vector<GLfloat> vertexData = std::vector<GLfloat>();
			unsigned int vertexCount = 0;
			GLuint vertexBufferObject = 0;
			GLuint vertexArrayObject = 0;
			GLuint texture = 0;
			GLuint textureUnit = baseTextureUnit++;

			objl::Mesh curMesh = Loader.LoadedMeshes[i];

			for (int j = 0; j < curMesh.Vertices.size(); j++)
			{
				vertexData.push_back(curMesh.Vertices[j].Position.X);
				vertexData.push_back(curMesh.Vertices[j].Position.Y);
				vertexData.push_back(curMesh.Vertices[j].Position.Z);
				vertexData.push_back(1.0f);

				vertexData.push_back(curMesh.Vertices[j].TextureCoordinate.X);
				vertexData.push_back(curMesh.Vertices[j].TextureCoordinate.Y);

				vertexData.push_back(curMesh.Vertices[j].Normal.X);
				vertexData.push_back(curMesh.Vertices[j].Normal.Y);
				vertexData.push_back(curMesh.Vertices[j].Normal.Z);
				vertexData.push_back(0.0f);

				vertexCount++;
			}

			createVertexArrayObject(vertexArrayObject, vertexBufferObject, texture, vertexData, textureUnit);

			Model* model = new Model(
				vertexData,
				vertexCount,
				vertexBufferObject,
				vertexArrayObject,
				texture,
				textureUnit
			);

			models.push_back(model);
		}

		return true;
	}

	GLuint ModelBuilder::readTexture(const char* filename, GLuint textureUnit)
	{
		GLuint texture;
		glActiveTexture(GL_TEXTURE0 + textureUnit);

		//Wczytanie do pami�ci komputera
		std::vector<unsigned char> image;   //Alokuj wektor do wczytania obrazka
		unsigned width, height;   //Zmienne do kt�rych wczytamy wymiary obrazka
		//Wczytaj obrazek
		unsigned error = lodepng::decode(image, width, height, filename);

		//Import do pami�ci karty graficznej
		glGenTextures(1, &texture); //Zainicjuj jeden uchwyt
		glBindTexture(GL_TEXTURE_2D, texture); //Uaktywnij uchwyt
		//Wczytaj obrazek do pami�ci KG skojarzonej z uchwytem
		glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*)image.data());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		return texture;
	}

	void ModelBuilder::createVertexArrayObject(
		GLuint& vertexArrayObject,
		GLuint& vertexBufferObject,
		GLuint& texture,
		std::vector<GLfloat> vertexData,
		GLuint textureUnit
	)
	{
		glGenVertexArrays(1, &vertexArrayObject);
		glBindVertexArray(vertexArrayObject);

		texture = readTexture(texturePaths.at(textureUnit), textureUnit);

		glGenBuffers(1, &vertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexData.size(), &(vertexData[0]), GL_STATIC_DRAW);

		glVertexAttribPointer(0, 4, GL_FLOAT, false, 10 * sizeof(GLfloat), (void*)0);//vertex
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, 10 * sizeof(GLfloat), (void*)(4 * sizeof(GLfloat)));//texCoord
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(2, 4, GL_FLOAT, false, 10 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));//normal
		glEnableVertexAttribArray(2);

		glBindVertexArray(0);
	}
}
