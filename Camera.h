#pragma once


#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <model.h>

class Camera
{
public:
	Camera();
	glm::mat4 getLookAtMatrix();
	glm::vec4 getPosition();
	glm::mat4 getPerspective(float aspectRatio);

	void moveForward(float change);
	void moveSideways(float change);
	void moveUp(float change);
	void rotateUp(float change);
	void rotateSideways(float change);
	void changeFov(float value);

protected:
	glm::vec3 cameraPosition;
	glm::vec3 cameraFrontVector;
	glm::vec3 cameraUpDirectionVector;

	float horizontalAngle;
	float verticalAngle;
	float fov;

	void updateCameraFrontVector();
};

