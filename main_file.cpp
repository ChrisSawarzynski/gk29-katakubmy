/*
Niniejszy program jest wolnym oprogramowaniem; możesz go
rozprowadzać dalej i / lub modyfikować na warunkach Powszechnej
Licencji Publicznej GNU, wydanej przez Fundację Wolnego
Oprogramowania - według wersji 2 tej Licencji lub(według twojego
wyboru) którejś z późniejszych wersji.

Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
ZASTOSOWAŃ.W celu uzyskania bliższych informacji sięgnij do
Powszechnej Licencji Publicznej GNU.

Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
Powszechnej Licencji Publicznej GNU(GNU General Public License);
jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
Place, Fifth Floor, Boston, MA  02110 - 1301  USA
*/

#define GLM_FORCE_RADIANS
#define GLM_FORCE_SWIZZLE

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include "constants.h"
#include "lodepng.h"
#include "shaderprogram.h"
#include <model.h>
#include <Camera.h>
#include <iostream>

float cameraSpeedForward = 0;
float cameraSpeedSideways = 0;
float cameraSpeedUpward = 0;

float cameraRotateSpeedUpward = 0;
float cameraRotateSpeedSideways = 0;

const float baseCameraRotateSpeed = PI / 4;
const float baseCameraSpeed = 5.0f;

float timeSinceLastFrame;

float aspectRatio = 1;

std::vector<Models::Model*> models;
ShaderProgram* sp;
ShaderProgram* sp_simple;
ShaderProgram* sp_adv;
Camera* camera;

Models::Model* map;


//Procedura obsługi błędów
void error_callback(int error, const char* description) {
	fputs(description, stderr);
}


void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_LEFT)
			cameraRotateSpeedSideways = -baseCameraRotateSpeed;
        if (key == GLFW_KEY_RIGHT)
			cameraRotateSpeedSideways = baseCameraRotateSpeed;
        if (key == GLFW_KEY_UP)
			cameraRotateSpeedUpward = baseCameraRotateSpeed;
        if (key == GLFW_KEY_DOWN)
			cameraRotateSpeedUpward = -baseCameraRotateSpeed;

		if (key == GLFW_KEY_W)
			cameraSpeedForward = baseCameraSpeed;
		if (key == GLFW_KEY_S)
			cameraSpeedForward = -baseCameraSpeed;
		if (key == GLFW_KEY_A)
			cameraSpeedSideways = -baseCameraSpeed;
		if (key == GLFW_KEY_D)
			cameraSpeedSideways = baseCameraSpeed;
		if (key == GLFW_KEY_Q)
			cameraSpeedUpward = baseCameraSpeed;
		if (key == GLFW_KEY_E)
			cameraSpeedUpward = -baseCameraSpeed;
		
		if (key == GLFW_KEY_LEFT_SHIFT)
			camera->changeFov(20.0f);

		if (key == GLFW_KEY_1)
			sp = sp_simple;
		if (key == GLFW_KEY_2)
			sp = sp_adv;
    }
    if (action == GLFW_RELEASE) {
		if (key == GLFW_KEY_LEFT)
			cameraRotateSpeedSideways = 0;
		if (key == GLFW_KEY_RIGHT)
			cameraRotateSpeedSideways = 0;
		if (key == GLFW_KEY_UP)
			cameraRotateSpeedUpward = 0;
		if (key == GLFW_KEY_DOWN)
			cameraRotateSpeedUpward = 0;

		if (key == GLFW_KEY_W)
			cameraSpeedForward = 0;
		if (key == GLFW_KEY_S)
			cameraSpeedForward = 0;
		if (key == GLFW_KEY_A)
			cameraSpeedSideways = 0;
		if (key == GLFW_KEY_D)
			cameraSpeedSideways = 0;
		if (key == GLFW_KEY_Q)
			cameraSpeedUpward = 0;
		if (key == GLFW_KEY_E)
			cameraSpeedUpward = 0;

		if (key == GLFW_KEY_LEFT_SHIFT)
			camera->changeFov(50.0f);
    }
}

void windowResizeCallback(GLFWwindow* window,int width,int height) {
    if (height==0) return;
    aspectRatio=(float)width/(float)height;
    glViewport(0,0,width,height);
}

//Procedura inicjująca
void initOpenGLProgram(GLFWwindow* window) {
	//************Tutaj umieszczaj kod, który należy wykonać raz, na początku programu************
	glClearColor(0,0,0,1);
	glEnable(GL_DEPTH_TEST);
	glfwSetWindowSizeCallback(window,windowResizeCallback);
	glfwSetKeyCallback(window,keyCallback);

	camera = new Camera();
	sp_simple = new ShaderProgram("Resources\\Shaders\\v_simplest.glsl",NULL,"Resources\\Shaders\\f_simplest.glsl");
	sp_adv = new ShaderProgram("Resources\\Shaders\\v_adv.glsl", NULL, "Resources\\Shaders\\f_adv.glsl");
	sp = sp_simple;


	std::vector<const char*> texturePaths = {
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\BROWN1.PNG",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\BROWN1.PNG",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\stone-wall-repeated.png",
		"Resources\\Textures\\bricks3b_diffuse.png",
		"Resources\\Textures\\bricks.png",
		"Resources\\Textures\\unnamed.png",
		"Resources\\Textures\\terrain colour.png",
		"Resources\\Textures\\unnamed.png"
	};
	Models::ModelBuilder modelBuilder = Models::ModelBuilder("Resources\\Models\\castle_v2.obj", texturePaths);
	models = modelBuilder.build();

	for (Models::Model* model : models)
	{
		model->setModelMatrix(
			glm::translate(model->getModelMatrix(), glm::vec3(-10.0f, -10.0f, 0.0f))
		);
	}
	
}


//Zwolnienie zasobów zajętych przez program
void freeOpenGLProgram(GLFWwindow* window) {
    //************Tutaj umieszczaj kod, który należy wykonać po zakończeniu pętli głównej************
	delete camera;
    delete sp_simple;
	delete sp_adv;

	for (Models::Model* model : models)
	{
		delete model;
	}
}




//Procedura rysująca zawartość sceny
void drawScene(GLFWwindow* window) {
	//************Tutaj umieszczaj kod rysujący obraz******************l
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	camera->moveForward(cameraSpeedForward * timeSinceLastFrame);
	camera->moveSideways(cameraSpeedSideways * timeSinceLastFrame);
	camera->moveUp(cameraSpeedUpward * timeSinceLastFrame);

	camera->rotateUp(cameraRotateSpeedUpward * timeSinceLastFrame);
	camera->rotateSideways(cameraRotateSpeedSideways * timeSinceLastFrame);

	glm::vec4 lightPosition = camera->getPosition() + glm::vec4(0, 3, 0, 1);
	glm::vec4 lightPosition2 = glm::vec4(-20.0f, 50.0f, -20.0f, 1.0f);

	glm::mat4 V = camera->getLookAtMatrix();
	glm::mat4 P = camera->getPerspective(aspectRatio);

    sp->use();

	glUniform4fv(sp->u("lightPosition"), 1, glm::value_ptr(lightPosition));
	glUniform4fv(sp->u("lightPosition2"), 1, glm::value_ptr(lightPosition2));

	for (Models::Model* model : models)
	{
		model->draw(sp, P, V);
	}

    glfwSwapBuffers(window);
}


int main(void)
{
	GLFWwindow* window; //Wskaźnik na obiekt reprezentujący okno

	glfwSetErrorCallback(error_callback);//Zarejestruj procedurę obsługi błędów

	if (!glfwInit()) { //Zainicjuj bibliotekę GLFW
		fprintf(stderr, "Nie można zainicjować GLFW.\n");
		exit(EXIT_FAILURE);
	}

	window = glfwCreateWindow(500, 500, "OpenGL", NULL, NULL);  //Utwórz okno 500x500 o tytule "OpenGL" i kontekst OpenGL.

	if (!window) //Jeżeli okna nie udało się utworzyć, to zamknij program
	{
		fprintf(stderr, "Nie można utworzyć okna.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window); //Od tego momentu kontekst okna staje się aktywny i polecenia OpenGL będą dotyczyć właśnie jego.
	glfwSwapInterval(1); //Czekaj na 1 powrót plamki przed pokazaniem ukrytego bufora

	if (glewInit() != GLEW_OK) { //Zainicjuj bibliotekę GLEW
		fprintf(stderr, "Nie można zainicjować GLEW.\n");
		exit(EXIT_FAILURE);
	}

	initOpenGLProgram(window); //Operacje inicjujące

	glfwSetTime(0); //Zeruj timer
	while (!glfwWindowShouldClose(window)) //Tak długo jak okno nie powinno zostać zamknięte
	{		
		timeSinceLastFrame = glfwGetTime();
        glfwSetTime(0); //Zeruj timer

		drawScene(window); //Wykonaj procedurę rysującą

		glfwPollEvents(); //Wykonaj procedury callback w zalezności od zdarzeń jakie zaszły.
	}

	freeOpenGLProgram(window);

	glfwDestroyWindow(window); //Usuń kontekst OpenGL i okno
	glfwTerminate(); //Zwolnij zasoby zajęte przez GLFW
	exit(EXIT_SUCCESS);
}
