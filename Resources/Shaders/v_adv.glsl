#version 330

//Zmienne jednorodne
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform vec4 lightPosition;
uniform vec4 lightPosition2;

//Atrybuty
layout(location = 0) in vec4 vertex;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec4 normal;

out vec2 iTexCoord;
out vec4 l;
out vec4 l2;
out vec4 n;
out vec4 v;

void main(void) {
    vec4 lp = lightPosition; //pozcyja �wiat�a, przestrze� �wiata
	vec4 lp2 = lightPosition2;

    l = normalize(V * lp - V * M * vertex); //wektor do �wiat�a w przestrzeni oka
	l2 = normalize(V * lp2 - V * M * vertex); //wektor do �wiat�a w przestrzeni oka

    v = normalize(vec4(0, 0, 0, 1) - V * M * vertex); //wektor do obserwatora w przestrzeni oka
    n = normalize(V * M * normal); //wektor normalny w przestrzeni oka

    iTexCoord = texCoord;

    gl_Position=P*V*M*vertex;
}
