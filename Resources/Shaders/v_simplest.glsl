#version 330

//Zmienne jednorodne
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform vec4 lightPosition;
uniform vec4 lightPosition2;

//Atrybuty
layout(location = 0) in vec4 vertex;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec4 normal;

out vec2 iTexCoord;

void main(void) {
    iTexCoord = texCoord;

    gl_Position=P*V*M*vertex;
}
