#version 330

uniform sampler2D textureMap;

in vec2 iTexCoord;

out vec4 pixelColor; //Zmienna wyjsciowa fragment shadera. Zapisuje sie do niej ostateczny (prawie) kolor piksela

void main(void) {
	pixelColor = texture(textureMap, iTexCoord);
}
