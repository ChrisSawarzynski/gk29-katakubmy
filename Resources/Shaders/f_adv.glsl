#version 330

uniform sampler2D textureMap;

in vec2 iTexCoord;
in vec4 l;
in vec4 l2;
in vec4 n;
in vec4 v;

out vec4 pixelColor; //Zmienna wyjsciowa fragment shadera. Zapisuje sie do niej ostateczny (prawie) kolor piksela

void main(void) {
	//Znormalizowane interpolowane wektory
	vec4 ml = normalize(l);
	vec4 ml2 = normalize(l2);

	vec4 mn = normalize(n);
	vec4 mv = normalize(v);
	//Wektor odbity
	vec4 mr = reflect(-ml, mn);

	vec4 kd = texture(textureMap, iTexCoord);

	vec4 ks = vec4(0, 1, 0.2, 1);
	vec4 ks2 = vec4(0.5f, 0.0f, 0.0f, 1);

	//Obliczenie modelu oświetlenia
	float nl = clamp(dot(mn, ml) * dot(mn, ml2), 0, 1);
	float rv = pow(clamp(dot(mr, mv), 0, 1), 50);

	pixelColor = (vec4(kd.rgb * nl, kd.a) + vec4(ks.rgb*rv, 0) + vec4(ks2.rgb*rv, 0));
}
