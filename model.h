/*
Niniejszy program jest wolnym oprogramowaniem; mo�esz go
rozprowadza� dalej i / lub modyfikowa� na warunkach Powszechnej
Licencji Publicznej GNU, wydanej przez Fundacj� Wolnego
Oprogramowania - wed�ug wersji 2 tej Licencji lub(wed�ug twojego
wyboru) kt�rej� z p�niejszych wersji.

Niniejszy program rozpowszechniany jest z nadziej�, i� b�dzie on
u�yteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domy�lnej
gwarancji PRZYDATNO�CI HANDLOWEJ albo PRZYDATNO�CI DO OKRE�LONYCH
ZASTOSOWA�.W celu uzyskania bli�szych informacji si�gnij do
Powszechnej Licencji Publicznej GNU.

Z pewno�ci� wraz z niniejszym programem otrzyma�e� te� egzemplarz
Powszechnej Licencji Publicznej GNU(GNU General Public License);
je�li nie - napisz do Free Software Foundation, Inc., 59 Temple
Place, Fifth Floor, Boston, MA  02110 - 1301  USA
*/

#ifndef MODEL_H
#define MODEL_H


#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include "constants.h"
#include "shaderprogram.h"

namespace Models {
	class Model
	{
	public:
		Model(
			std::vector<GLfloat> vertexData,
			unsigned int vertexCount,
			GLuint vertexBufferObject,
			GLuint vertexArrayObject,
			GLuint texture,
			GLuint textureUnit
		);
		~Model();
		void draw(ShaderProgram* sp, glm::mat4 P, glm::mat4 V);

		glm::mat4 getModelMatrix();
		void setModelMatrix(glm::mat4 M);

	protected:
		glm::mat4 M;

		std::vector<GLfloat> vertexData;
		unsigned int vertexCount;
		GLuint vertexBufferObject;
		GLuint vertexArrayObject;
		GLuint texture;
		GLuint textureUnit;

		void bind();
		void unbind();
	};


	class ModelBuilder
	{
	public:
		ModelBuilder(const char* modelPath, std::vector<const char*> texturePaths);
		std::vector<Model*> build();

	protected:
		const char* modelPath;
		std::vector<const char*> texturePaths;
		std::vector<Model*> models;

		bool loadVertexData();
		GLuint readTexture(const char* filename, GLuint textureUnit);
		void createVertexArrayObject(GLuint& vertexArrayObject, GLuint& vertexBufferObject, GLuint& texture, std::vector<GLfloat> vertexData, GLuint textureUnit);
	};
}

#endif
